FROM debian:latest

RUN apt-get update
RUN apt-get install -y gnupg2
RUN DEBIAN_FRONTEND=noninteractive
RUN apt-get update
RUN apt-get install -y\
 autoconf\
 automake\
 autopoint\
 bash\
 bison\
 bzip2\
 flex\
 g++\
 g++-multilib\
 gettext\
 git\
 gperf\
 intltool\
 libc6-dev-i386\
 libgdk-pixbuf2.0-dev\
 libltdl-dev\
 libssl-dev\
 libtool-bin\
 libxml-parser-perl\
 lzip\
 make\
 nsis\
 openssl\
 p7zip-full\
 patch\
 perl\
 python\
 ruby\
 sed\
 unzip\
 wget\
 xz-utils\
 gawk

USER root
WORKDIR /

RUN git clone https://gitlab.com/jak89_1/docker-testing.git opt/

RUN rm -rf /opt/.git/
RUN mv opt/* /
RUN rm -rf /opt/

RUN mkdir -p /build/
RUN mkdir -p /libs/dll/

RUN chmod +x mxe.sh && ./mxe.sh

